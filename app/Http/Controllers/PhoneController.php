<?php

namespace App\Http\Controllers;

use App\Http\Requests\Phone\StoreRequest;
use App\Http\Requests\Phone\UpdateRequest;
use App\Models\Phone;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    public function update(UpdateRequest $updateRequest, Phone $phone)
    {
        $phone->fill($updateRequest->validated());
        $phone->save();

        return redirect()->back();
    }

    public function store(StoreRequest $storeRequest, Phone $phone)
    {
        $phone->fill($storeRequest->validated());
        $phone->save();

        return redirect()->back();
    }

    public function destroy(Phone $phone)
    {
        $error = null;
        if($phone->where('phone_book_id', $phone->phone_book_id)->count() > 1){
            $phone->delete();
        }else{
            $error['last_phone'.$phone->id] = 'This number is not deleted, because it is the last number for the user.';
        }

        return redirect()->back()->withErrors($error);
    }
}
