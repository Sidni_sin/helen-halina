<?php

namespace App\Http\Controllers;

use App\Models\PhoneBook;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(PhoneBook $phoneBook)
    {
        $phoneBooks = $phoneBook->with('phones')->get();

        return view('main', compact('phoneBooks'));
    }
}
