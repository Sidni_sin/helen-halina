<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhoneBook\StoreRequest;
use App\Http\Requests\PhoneBook\UpdateRequest;
use App\Models\Phone;
use App\Models\PhoneBook;
use Illuminate\Http\Request;
use DB;

class PhoneBookController extends Controller
{
    public function store(StoreRequest $storeRequest, PhoneBook $phoneBook, Phone $phone)
    {
        $phoneBook->fill($storeRequest->validated());
        $phone->fill($storeRequest->validated());

        DB::transaction(function () use ($phoneBook, $phone){
            $phoneBook->save();
            $phoneBook->phones()->save($phone);
        });

        return redirect()->back();
    }

    public function update(UpdateRequest $updateRequest, PhoneBook $phoneBook)
    {
        $phoneBook->fill($updateRequest->validated());

        $phoneBook->save();

        return redirect()->back();
    }

    public function destroy(PhoneBook $phoneBook)
    {
        $phoneBook->phones()->delete();
        $phoneBook->delete();

        return redirect()->back();
    }
}
