<?php

namespace App\Http\Requests\Phone;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone' => ['required', 'regex:/^(380|0)[0-9]{9}$/', 'unique:phones'],
            'phone_book_id' => 'exists:phones',
        ];
    }
}
