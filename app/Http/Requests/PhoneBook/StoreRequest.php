<?php

namespace App\Http\Requests\PhoneBook;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_name'    => 'required|string|size:255',
            'middle_name'   => 'required|string|size:255',
            'last_name'     => 'required|string|size:255',
            'phone'         => ['required', 'regex:/(380|)[0-9]{9}/', 'unique:phones'],
        ];
    }
}
