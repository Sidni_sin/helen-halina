<?php

namespace App\Http\Requests\PhoneBook;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_name'    => 'required|string|size:255',
            'middle_name'   => 'required|string|size:255',
            'last_name'     => 'required|string|size:255',
        ];
    }
}
