<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhoneBook extends Model
{
    protected $fillable = ['first_name', 'middle_name', 'last_name'];

    public function phones()
    {
        return $this->hasMany(Phone::class);
    }
}
