<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ['phone', 'phone_book_id'];

    public function phoneBook()
    {
        return $this->belongsTo(PhoneBook::class);
    }
}
