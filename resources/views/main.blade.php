@extends('layouts.app')

@section('content')
    <div class="col-md-4">
        <form action="{{route('phone-book.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">First name</label>
                <input type="text" class="form-control" name="first_name" placeholder="Peter">
            </div>
            @if($errors->has('first_name'))
                <div class="invalid-feedback" style="display: block;">
                    {{$errors->first('first_name')}}
                </div>
            @endif
            <div class="form-group">
                <label for="exampleInputPassword1">Middle name</label>
                <input type="text" class="form-control" name="middle_name" placeholder="Jone">
            </div>
            @if($errors->has('middle_name'))
                <div class="invalid-feedback" style="display: block;">
                    {{$errors->first('middle_name')}}
                </div>
            @endif
            <div class="form-group">
                <label for="exampleInputPassword1">Last name</label>
                <input type="text" class="form-control" name="last_name" placeholder="White">
            </div>
            @if($errors->has('last_name'))
                <div class="invalid-feedback" style="display: block;">
                    {{$errors->first('last_name')}}
                </div>
            @endif
            <div class="form-group">
                <label for="exampleInputPassword1">Phone</label>
                <input type="number" class="form-control" name="phone" placeholder="380662243712">
            </div>
            @if($errors->has('phone'))
                <div class="invalid-feedback" style="display: block;">
                    {{$errors->first('phone')}}
                </div>
            @endif
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <div class="col-md-8">
        <div class="row">
            @foreach($phoneBooks as $phoneBook)
                <div class="col-md-12 pb-5">
                    <div>
                        <b>{{$phoneBook->first_name}} {{ $phoneBook->middle_name }} {{ $phoneBook->last_name }}</b>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$phoneBook->id}}">
                            edit user
                        </button>
                        <form action="{{route('phone-book.destroy', $phoneBook->id)}}" method="POST" style="display: inline-block;">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" type="submit">delete user</button>
                        </form>
                    </div>

                    <div class="modal fade" id="exampleModal{{$phoneBook->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="{{route('phone-book.update', $phoneBook->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First name</label>
                                            <input type="text" class="form-control" name="first_name" value="{{$phoneBook->first_name}}" placeholder="Peter">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Middle name</label>
                                            <input type="text" class="form-control" name="middle_name" value="{{$phoneBook->middle_name}}" placeholder="Jone">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Last name</label>
                                            <input type="text" class="form-control" name="last_name" value="{{$phoneBook->last_name}}" placeholder="White">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group">
                        @foreach($phoneBook->phones as $phone)
                            <li class="list-group-item">
                                {{$phone->phone}}

                                <form action="{{route('phone.destroy', $phone->id)}}" class="float-right" method="POST" style="display: inline-block;">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">delete number</button>
                                </form>

                                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal{{$phoneBook->id}}{{$phone->id}}">
                                    edit phone
                                </button>


                                @if($errors->has('last_phone'.$phone->id))
                                    <div class="invalid-feedback" style="display: block;">
                                        {{$errors->first('last_phone'.$phone->id)}}
                                    </div>
                                @endif
                            </li>
                            <div class="modal fade" id="exampleModal{{$phoneBook->id}}{{$phone->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form action="{{route('phone.update', $phone->id)}}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Изменить номер</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Phone</label>
                                                    <input type="number" class="form-control" name="phone" value="{{$phone->phone}}" placeholder="380662243712">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <li class="list-group-item">
                            Create New Phone for user <b>{{$phoneBook->first_name}}</b>
                            <form action="{{route('phone.store')}}" method="POST">
                                @csrf
                                <input type="number" name="phone_book_id" value="{{$phoneBook->id}}" style="display: none;">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="phone" placeholder="380662243712">
                                </div>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                            @if($errors->has('phone'))
                                <div class="invalid-feedback" style="display: block;">
                                    {{$errors->first('phone')}}
                                </div>
                            @endif
                        </li>
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
@endsection